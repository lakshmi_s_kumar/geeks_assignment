// import React from 'react';
// // import logo from './logo.svg';
// import './App.css';
// // import Jsonpdf from './component/Jsonpdf';
// // import Example from './component/Collapse';
// import Datepicker from "./component/Datepicker"




// function App() {
//   return (
//     <div className="App">
//       {/* <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header> */}
//       {/* <Jsonpdf/> */}
//       {/* <Example/> */}
//       <Datepicker />
      
      
//     </div>
//   );
// }

// export default App;
// import React from 'react';
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <h3>Build Sign Up & Login UI Template in React</h3>
//     </div>
//   );
// }

// export default App;
import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Login from "./component/login";
import SignUp from "./component/signup";
import CompanyInfo from "./component/CompanyInfo"

function App() {
  return (<Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/company-info"}>Company Info</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-in"}>Login</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/sign-in" component={Login} />
            <Route path="/sign-up" component={SignUp} />
            <Route path="/company-info" component={CompanyInfo} />
          </Switch>
        </div>
      </div>
    </div></Router>
  );
}

export default App;